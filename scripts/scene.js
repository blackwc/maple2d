maple.scene = function(props, c) {

	this.core = c;

	this.property = {
		name: "",
		gravity: null
	}

	for(var foo in props) {
		this.property[foo] = props[foo];
	}


	this.renderGroup = {};
	this.max_z = 0;
	this.world = null;
	this.lighting = null;
	this.light = null;
	this.darkmask = null;
	this.create();
}

maple.scene.prototype.create = function() {

	var self = this.self();

	var scn = self.core.game.scenes[self.property.name];

	var gx = 0;
	var gy = 0;

	if (_item_in_keys("gravity",self.core.game.scenes[self.property.name])) {
		gx = self.core.game.scenes[self.property.name]["gravity"][0];
		gy = self.core.game.scenes[self.property.name]["gravity"][1];
	}

	self.property.gravity = new maple.position(gx,gy);

	self.world = new b2World(new b2Vec2(self.property.gravity.x,
		self.property.gravity.y),true);

	
	self.light = new Lamp({
	    position: new Vec2(100, 220),
	    distance: 250,
	    samples: 25,
	    color: "RGBA(255,255,255,1)",
	    diffuse: 0.8,
	    size: 10,
	    radius: 10
	  });

	var debugDraw = new b2DebugDraw();
	debugDraw.SetSprite(self.core.context);
	debugDraw.SetDrawScale(35);
	debugDraw.SetFillAlpha(0.3);
	debugDraw.SetLineThickness(1.0);
	debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
	self.world.SetDebugDraw(debugDraw);

	//TODO
	self.core.property.origin.x = scn.origin[0];
	self.core.property.origin.y = scn.origin[1];
	self.core.context.translate(self.core.property.origin.x,
		-1*self.core.property.origin.y);

	for (var gname in scn.graphics) {

		var g = new maple.graphic(self.core.resources.graphics[gname],
			gname, self.core);
		for (var i in scn.graphics[gname]) {

			var tgname = scn.graphics[gname][i].name;
			var tile = scn.graphics[gname][i].tile;

			var rr = [];
			for(var a in scn.graphics[gname][i].pos) {
				var b = scn.graphics[gname][i].pos[a];
				var r = new maple.rect(b[0],b[1],tile[2],tile[3]);
				rr.push(r);
			}

			var tg = new maple.tile.group({
				name: tgname,
				graphic: g,
				tile: new maple.rect(tile[0],tile[1],tile[2],tile[3]),
				zindex: scn.graphics[gname][i].z,
				rects: rr
			}, self.core);

			self.max_z = Math.max(self.max_z, tg.property.zindex);
			
			if(!_item_in_keys(String(tg.property.zindex),self.renderGroup))
				self.renderGroup[String(tg.property.zindex)] = {};

			if(!_item_in_keys(tgname,self.renderGroup[
				String(tg.property.zindex)
				]))
				self.renderGroup[String(tg.property.zindex)][tgname] = [];
			
			self.renderGroup[String(tg.property.zindex)][tgname].push(tg);
		}
	}

	for (var n = 0; n < scn.objects.length; n++) {
		var oname = scn.objects[n].name;

		var g = null;
		if (_item_in_keys("graphic",self.core.game.objects[oname])) {
			var gname = self.core.game.objects[oname].graphic;
			
			var g = new maple.graphic(self.core.resources.graphics[gname],
				gname, self.core);
		}


		var ss = new maple.size(scn.objects[n].size[0],
			scn.objects[n].size[1]);

		var rr = []
		for(var a in scn.objects[n].pos) {
			var b = scn.objects[n].pos[a];

			if(typeof(b) == "string") {
				//TODO
			} else {
				var r = new maple.rect(b[0] + ss.width/2,
					b[1]+ ss.height/2, ss.width, ss.height);
				rr.push(r);
			}
		}

		var obj = new maple.object({
			name:oname,
			graphic:g,
			rects: rr,
			size: ss,
			zindex: scn.objects[n].z
		},self.world,self.core);

		self.max_z = Math.max(self.max_z,obj.property.zindex);

		if(!_item_in_keys(String(obj.property.zindex),self.renderGroup))
			self.renderGroup[String(obj.property.zindex)] = {};
		if(!_item_in_keys(obj.property.name,
			self.renderGroup[String(obj.property.zindex)]))
			self.renderGroup[
				String(obj.property.zindex)
			][obj.property.name] = [];

		self.renderGroup[String(obj.property.zindex)][
					obj.property.name
			].push(obj);

		if(_item_in_keys("dynamic", self.core.game.objects[oname])) {
			obj.dynamic = self.core.game.objects[oname].dynamic;
		}

		self.core.objects[obj.property.name] = obj;
		var cobjs = self.core.game.objects[oname];

		if(_item_in_keys("map",cobjs)) {
			for (var l = 0; l < cobjs.map.length; l++) {
				for (var evt in cobjs.map[l]) {
					for (var acts in cobjs.map[l][evt]) {

						if (cobjs.map[l][evt][acts]) {
							var evt_args = cobjs.map[l][evt][acts][0];
						
							for(var x = 1;
								x < cobjs.map[l][evt][acts].length; x++) {
								
								if (cobjs.map[l][evt][acts][x]) {
									the_act = cobjs.map[l][evt][acts][x][0];
									the_act_args =
										cobjs.map[l][evt][acts][x].slice(1,
										cobjs.map[l][evt][acts][x].length);
									
									obj.link(_event_action_map(evt),
										_event_action_map(the_act),
										the_act_args,evt_args);

									self.core.log("map object '"+
										obj.property.name+"' "+
										evt+"(" + evt_args +") " +
										the_act + "("+the_act_args+")");
								}
							}
						}
					}
				}
			}
		}

		obj.create()
	}
}

maple.scene.prototype.loop = function() {

	var self = this.self();

	self.core.context.fillStyle = self.core.property.backgroundColor;

	self.core.context.fillRect(self.core.property.origin.x,
		self.core.property.origin.y,self.core.property.view.width,
		self.core.property.view.height);

	for(var z1 = 0; z1 <= self.max_z; z1++) {

		var z = String(z1);
		if(_item_in_keys(z,self.renderGroup)) {

			for (var i in self.renderGroup[z]) {
				for(var x = 0; x < self.renderGroup[z][i].length; x++) {
					if(self.renderGroup[z][i][x].onLogic)
						self.renderGroup[z][i][x].onLogic();
					if(self.renderGroup[z][i][x].render)
						self.renderGroup[z][i][x].render(self.core.context);
				}
			}
		}
	}

	
	if(_item_in_keys("mario",self.core.objects)) {
		var rect = new RectangleObject({ 
	    topleft: new Vec2(
	    	self.core.objects.mario.property.rects[0].position.x +
	    	self.core.objects.mario.property.size.width/2,
	    	self.core.objects.mario.property.rects[0].position.y +
	    	self.core.objects.mario.property.size.height/2), 
	    bottomright: new Vec2(
	    	self.core.objects.mario.property.rects[0].position.x +
	    	self.core.objects.mario.property.size.width/2 +
	    	self.core.objects.mario.property.size.width,
	    	self.core.objects.mario.property.rects[0].position.y +
	    	self.core.objects.mario.property.size.height/2 +
	    	self.core.objects.mario.property.size.height) 
		  });

		var rect1 = new RectangleObject({ 
	    topleft: new Vec2(
	    	self.core.objects.turtle.property.rects[0].position.x +
	    	self.core.objects.turtle.property.size.width/2,
	    	self.core.objects.turtle.property.rects[0].position.y +
	    	self.core.objects.turtle.property.size.height/2), 
	    bottomright: new Vec2(
	    	self.core.objects.turtle.property.rects[0].position.x+
	    	self.core.objects.turtle.property.size.width/2 +
	    	self.core.objects.turtle.property.size.width,
	    	self.core.objects.turtle.property.rects[0].position.y +
	    	self.core.objects.turtle.property.size.height/2 +
	    	self.core.objects.turtle.property.size.height) 
		  });

		self.lighting = new Lighting({
	    light: self.light,
	    objects: [rect,rect1]
	  	});

		self.darkmask = new DarkMask({ color: "RGBA(0,0,0,.8)",
			lights: [self.light] });

		if(self.lighting) {
			

			self.darkmask.compute(self.core.property.origin.x +
				self.core.property.view.width, self.core.property.origin.y +
				self.core.property.view.height);

			self.lighting.compute(self.core.property.origin.x +
				self.core.property.view.width, self.core.property.origin.y +
				self.core.property.view.height);

			self.core.context.globalCompositeOperation = "lighter";
			
		  	self.lighting.render(self.core.context);
		  	
		  	self.core.context.globalCompositeOperation = "source-over";
		  	self.darkmask.render(self.core.context);
		} 
	}

	//self.world.DrawDebugData();
	self.world.Step(1 / 60, 10, 10);
	self.world.ClearForces();
}

maple.scene.prototype.self = function() {
	return this;
}