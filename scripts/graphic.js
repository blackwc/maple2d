maple.graphic = {};
maple.tile = {};

maple.graphic = function(res, name, c) {

	if(res) {
		this.img = new Image();
		this.img.src = res;
	}
	this.name = name;

	this.core = c;
}

maple.tile.group = function(props, c) {

	this.core = c;

	this.property = {
		name: "",
		rects: [],
		graphic: null,
		tile: null,
		zindex: 0
	}

	for(var foo in props) {
		this.property[foo] = props[foo];
	}
}

maple.tile.group.prototype.render = function(context) {

	var self = this.self();
	if(!self.property.tile||!self.property.graphic) return;

	for (var i = 0; i < self.property.rects.length; i++) {
		
		context.drawImage(self.property.graphic.img,
			self.property.tile.position.x,
			self.property.tile.position.y,
			self.property.tile.size.width,
			self.property.tile.size.height,

			self.property.rects[i].position.x,self.property.rects[i].position.y,
			
			self.property.rects[i].size.width,
			self.property.rects[i].size.height);
	}
}

maple.tile.group.prototype.self = function() {
	return this;
}