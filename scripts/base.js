if(typeof maple == "undefined" || !maple) {
	var maple = {};
}

var DO = {
	MOVE: 2,
	TIMER: 3,
	ANIMATION: 5,
	TIMER_STOP: 6,
	SET_FRAME: 7,
	SET_VARIABLE: 8,
	IF: 9,
	SCENE: 10,
	SOUND: 11,
	TRANSLATE: 12,
	ALERT: 13,
	NOTHING: 14,
	SET_GRAVITY: 15,
	FOLLOW: 16,
	TRACE: 17,
	TRACE_ARRAY: 18
}

var ON = {
	LOGIC: 1,
	CREATE: 2,
	KEYHOLD: 3,
	KEYDOWN: 4,
	TIMER: 5,
	KEYUP: 6,
	TIMER_DONE: 7,
	TIMER_START: 8,
	COLLISION: 9,
	IF: 10
}

var KEY = {
	RIGHT_ARROW: 39,
	LEFT_ARROW: 37,
	SPACE: 32,
	UP_ARROW: 38,
	W: 87,
	S: 83,
	A: 65,
	D: 68
}

var IF = {
	EQUAL: 1,
	EQUAL_NOT: 5,
	GREATER: 2,
	GREATER_EQUAL: 3,
	LESS_EQUAL: 4,
	LESS: 6
}

maple.keymap = function() {
	
	this.fired = false;
	this.keys = [];
	this.event = function() {}
}

maple.position = function(x,y) {
	this.x = x;
	this.y = y;
}

maple.size = function(width,height) {
	this.width = width;
	this.height = height;
}

maple.rect = function(x,y,width,height) {

	this.position = new maple.position(x,y);
	this.size = new maple.size(width,height);

	var self = this;

	this.getRight = function() {
		return self.position.x+self.size.width;
	}

	this.getBottom = function() {
		return self.position.y+self.size.height;
	}

	this.getTop = function() {
		return self.position.y;
	}

	this.getLeft = function() {
		return self.position.x;
	}

	this.contains = function(x,y) {

		var ret1 = (x >= self.position.x);
		var ret2 = (y >= self.position.y);

		var ret3 = (x < self.getRight());
		var ret4 = (y < self.getBottom());

		if (ret1 && ret2 && ret3 && ret4) return true;

		return false;
	}

	this.deflate = function(n) {
		self.position.x = self.position.x+n;
		self.position.y = self.position.y+n;
		self.size.width = self.size.width-n*2;
		self.size.height = self.size.height-n*2;
	}

	this.inflate = function(n) {
		self.position.x  = self.position.x-n;
		self.position.y  = self.position.y-n;
		self.size.width  = self.size.width+n*2;
		self.size.height = self.size.height+n*2;
	}
}

function _event_action_map(b) {
	b = String(b).split(".");
	var d = null;
	if(b[0] == "ON") d = ON;
	if(b[0] == "DO") d = DO;
	if(b[0] == "KEY") d = KEY;
	if(b[0] == "COL") d = COL;
	if(b[0] == "IF") d = IF;
	return d[b[1]];
}

function _item_in_keys(x, d) {

	if(Object.keys(d).indexOf(x) != -1)
		return true;
	else
		return false;
}

function run(g) {

	var core = null;

	window.onload = function(e) {
		core = new maple.core(g);
	}

	document.onkeydown = function(e) {
		if(core) return core.onkeydown(e);
	}

	document.onkeyup = function(e) {
		if(core) return core.onkeyup(e);
	}
}

var   b2Vec2 = Box2D.Common.Math.b2Vec2
         	,	b2BodyDef = Box2D.Dynamics.b2BodyDef
         	,	b2Body = Box2D.Dynamics.b2Body
         	,	b2FixtureDef = Box2D.Dynamics.b2FixtureDef
         	,	b2Fixture = Box2D.Dynamics.b2Fixture
         	,	b2World = Box2D.Dynamics.b2World
         	,	b2MassData = Box2D.Collision.Shapes.b2MassData
         	,	b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
         	,	b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
         	,	b2DebugDraw = Box2D.Dynamics.b2DebugDraw
            ;

var Lamp = illuminated.Lamp
  , RectangleObject = illuminated.RectangleObject
  , DiscObject = illuminated.DiscObject
  , Vec2 = illuminated.Vec2
  , Lighting = illuminated.Lighting
  , DarkMask = illuminated.DarkMask
  ;

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();