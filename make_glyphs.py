import sys, os, glob

if sys.argv[1]:

	pname = os.path.basename(os.path.normpath(sys.argv[1])).upper()
	print '#ifndef H_RES_'+pname
	print '#define H_RES_'+pname

	for infile in glob.glob(os.path.join(sys.argv[1],'*.*')):
	
		print 'static const unsigned char %s[] = {'\
		 % os.path.basename(infile).replace('.','_')
	
		f = open(infile,'rb')
		
		tick = 1
		foo = ''
		while 1:
		
			b = f.read(1)
			if not b:
				foo = foo[:-2]
				break
			
			b = str(hex(ord(b)))
			
			e = b[2:]
			
			if len(e) == 1:
				e = '0'+e
			
			b = '0x'+e.upper()+', '
			
			foo += b
			
			if not tick % 10:
				foo += '\n'
				
			tick+=1
			
		f.close()
		
		print foo
		print '};\n'

	print '#endif\n'