#include "project_tree.h"
#include "main.h"

project_tree::project_tree(wxWindow *parent):
wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(225, 400)) {

	wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
	tree = new wxTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
		wxBORDER_THEME | wxNO_FULL_REPAINT_ON_RESIZE | wxCLIP_CHILDREN|
		wxTR_MULTIPLE| wxTR_HAS_BUTTONS|wxTR_FULL_ROW_HIGHLIGHT|wxTR_NO_LINES);
	sizer->Add(tree, 1, wxEXPAND);
	SetSizer(sizer);
}