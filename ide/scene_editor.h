#ifndef H_SCENE_EDITOR
#define H_SCENE_EDITOR

#include "std.h"

struct graphic {
	int x;
	int y;
	int z;
	int w;
	int h;
	wxImage img;
};

class scene_editor : public wxScrolledWindow {
private:
	Json::Value root;
	//TODO HASH MAP
	typedef std::pair<wxString, graphic> graphic_map;
	std::vector<graphic_map> graphics;
public:
	scene_editor(wxWindow *parent, wxString name, Json::Value _root);
	void onRender(wxPaintEvent &event);
};

#endif