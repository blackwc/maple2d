#ifndef H_DOCKART
#define H_DOCKART

#include "std.h"
#include <wx/aui/framemanager.h>
#include <wx/renderer.h>

class wxAuiCommandCapture : public wxEvtHandler
{
private:
    int m_lastId;
public:

    wxAuiCommandCapture() { m_lastId = 0; }
    int GetCommandId() const { return m_lastId; }

    bool ProcessEvent(wxEvent& evt)
    {
        if (evt.GetEventType() == wxEVT_COMMAND_MENU_SELECTED)
        {
            m_lastId = evt.GetId();
            return true;
        }

        if (GetNextHandler())
            return GetNextHandler()->ProcessEvent(evt);

        return false;
    };

};

class dockart: public wxAuiDefaultDockArt {
public:
	void InitBitmaps ();
	dockart();
	void DrawPaneButton(wxDC& dc, wxWindow *WXUNUSED(window),
                                      int button,
                                      int button_state,
                                      const wxRect& _rect,
                                      wxAuiPaneInfo& pane);
};

#endif