#include "main.h"

main::main(const wxString& title):
wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600)) {

	//TODO: LOCALE
	locale = new wxLocale(wxLANGUAGE_DEFAULT);
	locale->AddCatalogLookupPathPrefix(wxT("locale"));
	locale->AddCatalog(wxT("maple2d"));

	wxFileConfig *conf = Config();

	mgr.SetManagedWindow(this);
	mgr.SetFlags(wxAUI_MGR_HINT_FADE | wxAUI_MGR_TRANSPARENT_HINT | wxAUI_MGR_ALLOW_FLOATING);

	mgr.SetArtProvider(new dockart());

	wxMenu *file_menu = new wxMenu();
	file_menu->Append(MENU_NEW, wxT("&New"));
	file_menu->Append(MENU_OPEN, wxT("&Open..."));
	file_menu->AppendSeparator();
	file_menu->Append(MENU_SAVE, wxT("&Save"));
	file_menu->Append(MENU_SAVEAS, wxT("Save &As..."));
	file_menu->Append(MENU_SAVEALL, wxT("Save A&ll"));
	file_menu->AppendSeparator();
	file_menu->Append(MENU_EXIT, wxT("E&xit\t"));

	emitter_add_menu = new wxMenu();
	
	wxMenuItem *menu_add_light = new wxMenuItem(NULL, MENU_ADD_LIGHT, wxT("Add &Light..."));
	menu_add_light->SetBitmap(wxMEMORY_BITMAP(light_add_png));
	wxMenuItem *menu_add_particle = new wxMenuItem(NULL, MENU_ADD_PARTICLE, wxT("Add &Particle..."));
	menu_add_particle->SetBitmap(wxMEMORY_BITMAP(particle_add_png));
	wxMenuItem *menu_add_soundfx = new wxMenuItem(NULL, MENU_ADD_SOUNDFX, wxT("Add &SoundFX..."));
	menu_add_soundfx->SetBitmap(wxMEMORY_BITMAP(soundfx_add_png));

	emitter_add_menu->Append(menu_add_light);
	emitter_add_menu->Append(menu_add_particle);
	emitter_add_menu->Append(menu_add_soundfx);

	wxMenuBar *menubar = new wxMenuBar();
	menubar->Append(file_menu, wxT("File"));
	SetMenuBar(menubar);

	wxAuiToolBar *file_tb = new wxAuiToolBar(this);
	file_tb->AddTool(MENU_NEW, wxT("New..."), wxMEMORY_BITMAP(new_png), wxT("New..."));
	file_tb->AddTool(MENU_OPEN, wxT("Open.."), wxMEMORY_BITMAP(open_png), wxT("Open..."));
	file_tb->AddSeparator();
	file_tb->AddTool(MENU_SAVE, wxT("Save"), wxMEMORY_BITMAP(save_png), wxT("Save"));
	file_tb->AddTool(MENU_SAVEAS, wxT("Save As..."), wxMEMORY_BITMAP(save_as_png), wxT("Save As..."));
	file_tb->AddTool(MENU_SAVEAS, wxT("Save All"), wxMEMORY_BITMAP(save_all_png), wxT("Save All"));
	file_tb->Realize();

	project_tb = new wxAuiToolBar(this);
	project_tb->AddTool(MENU_ADD_GRAPHIC, wxT("Add Graphic"), wxMEMORY_BITMAP(graphic_add_png), wxT("Add Graphic"));
	project_tb->AddTool(MENU_ADD_SOUND, wxT("Add Sound"), wxMEMORY_BITMAP(sound_add_png), wxT("Add Sound"));
	project_tb->AddTool(MENU_ADD_OBJECT, wxT("Add Object"), wxMEMORY_BITMAP(object_add_png), wxT("Add Object"));
	project_tb->AddTool(MENU_ADD_EMITTER, wxT("Add Emitter"), wxMEMORY_BITMAP(emitter_add_png), wxT("Add Emitter"));
	project_tb->AddTool(MENU_ADD_SCENE, wxT("Add Scene"), wxMEMORY_BITMAP(scene_add_png), wxT("Add Scene"));
	project_tb->AddSeparator();
	project_tb->AddTool(MENU_ADD_FILTER, wxT("Add Filter"), wxMEMORY_BITMAP(folder_add_png), wxT("Add Filter"));
	project_tb->Realize();

	Bind(wxEVT_COMMAND_MENU_SELECTED, &main::onCommand, this);

	wxFont aui_font(7, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);

	mgr.GetArtProvider()->SetFont(wxAUI_DOCKART_CAPTION_FONT, aui_font);
	mgr.GetArtProvider()->SetMetric(wxAUI_DOCKART_PANE_BORDER_SIZE, 1);

	main_notebook = new wxAuiNotebook(this, -1, wxDefaultPosition, wxDefaultSize,
	wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS |
	wxAUI_NB_WINDOWLIST_BUTTON  | wxAUI_NB_CLOSE_ON_ALL_TABS | wxAUI_NB_TOP | wxBORDER_NONE | wxNO_BORDER);

	main_notebook->SetArtProvider(new tabart());

	ptree = new project_tree(this);

	prop_grid = new properties_grid(this);

	// add the panes to the manager
	mgr.AddPane(ptree, wxAuiPaneInfo().Name(wxT("ptree")).Caption(wxT("Project")).Left().PaneBorder(false));
	mgr.AddPane(main_notebook, wxAuiPaneInfo().Name(wxT("main_notebook")).CenterPane().PaneBorder(false));
	mgr.AddPane(prop_grid, wxAuiPaneInfo().Name(wxT("prop_grid")).Caption(wxT("Properties")).Left().PaneBorder(false));

	mgr.AddPane(file_tb, wxAuiPaneInfo().Name(wxT("file_tb")).Caption(wxT("File Toolbar")).Top().ToolbarPane().Gripper().Layer(2));
	mgr.AddPane(project_tb, wxAuiPaneInfo().Name(wxT("project_tb")).Caption(wxT("Project Toolbar")).Top().ToolbarPane().Gripper().Layer(2));
	mgr.Update();

	//load perspective
#ifndef _DEBUG
	wxString cPerspective;
	conf->Read(wxT("general/perspective"), &cPerspective, mgr.SavePerspective());
	mgr.LoadPerspective(cPerspective, true);
#endif

	SetIcon(wxIcon("APP_ICON"));

	wxImageList *img_list = new wxImageList(16, 16);
	img_list->Add(wxMEMORY_IMAGE(graphic_png));
	img_list->Add(wxMEMORY_IMAGE(sound_png));
	img_list->Add(wxMEMORY_IMAGE(object_png));
	img_list->Add(wxMEMORY_IMAGE(scene_png));
	img_list->Add(wxMEMORY_IMAGE(graphic_item_png));
	img_list->Add(wxMEMORY_IMAGE(sound_item_png));
	img_list->Add(wxMEMORY_IMAGE(object_item_png));
	img_list->Add(wxMEMORY_IMAGE(scene_item_png));
	img_list->Add(wxMEMORY_IMAGE(logo_png));
	img_list->Add(wxMEMORY_IMAGE(folder_png));
	img_list->Add(wxMEMORY_IMAGE(light_png));
	img_list->Add(wxMEMORY_IMAGE(particle_png));
	img_list->Add(wxMEMORY_IMAGE(soundfx_png));
	img_list->Add(wxMEMORY_IMAGE(emitter_png));

	ptree->tree->SetImageList(img_list);
	main_notebook->SetImageList(img_list);

	//ready props
	wxString pmeta_path = wxFileName(wxStandardPaths::Get().GetExecutablePath()).GetPath() + wxT("/props.meta");
	wxFileName fmp(pmeta_path);
	if (fmp.Exists()) {
		Json::Reader reader;
		std::ifstream pmeta_doc(fmp.GetFullPath().c_str(), std::ifstream::binary);
		bool ret = reader.parse(pmeta_doc, props_json);
		if (!ret) {
			wxLogError(wxString(reader.getFormattedErrorMessages()));
			wxLogError(wxT("Parsing JSON in file '" + fmp.GetFullName() + "' failed."));
		}

		//TODO do something with props.meta
	}

	Bind(wxEVT_SIZE, &main::OnSize, this);

	ptree->Bind(wxEVT_TREE_ITEM_ACTIVATED, &main::onProjectTreeItemActivate, this);

	//TODO: center and size should be first thing but location and size and if maximized or not should be read from config
	wxDisplay dis;
	wxRect rect = dis.GetClientArea();
	SetSize(rect.GetWidth(), rect.GetHeight());
	Center();

	delete conf;

	cacheProps();

	Show();
}

void main::onSize(wxSizeEvent &WXUNUSED(event)) {
}

void main::onPageClosed(wxAuiNotebookEvent &WXUNUSED(event)) {

}

void main::onPageChanged(wxAuiNotebookEvent &WXUNUSED(event)) {

}

void main::onCommand(wxCommandEvent &event) {

	switch (event.GetId()) {
	case MENU_NEW:
		doNew();
		return;
	case MENU_OPEN:
		doOpen();
		return;
	case MENU_EXIT:
		Close();
		return;
	case MENU_ADD_EMITTER:
		wxRect r = project_tb->GetToolRect(event.GetId());
		wxAuiPaneInfo pane = mgr.GetPane(wxT("project_tb"));
		wxPoint pt = pane.rect.GetBottomLeft();
		if (!pane.IsFloating()) {
			switch (pane.dock_direction) {
			case wxAUI_DOCK_TOP:
				pt = pane.rect.GetBottomLeft();
				pt = wxPoint(pt.x + r.x, pt.y);
				break;
			case wxAUI_DOCK_LEFT:
				pt = pane.rect.GetTopRight();
				pt = wxPoint(pt.x + r.x, pt.y + r.GetTop());
				break;
			case wxAUI_DOCK_RIGHT:
				pt = pane.rect.GetTopLeft();
				pt = wxPoint(pt.x + r.x, pt.y + r.GetTop());
				break;
			case wxAUI_DOCK_BOTTOM:
				pt = pane.rect.GetTopLeft();
				pt = wxPoint(pt.x + r.x, pt.y);
				break;
			}
			
		} else {
			pt = pane.floating_pos;
			pt = wxPoint(pt.x + r.x, pt.y);
		}
		PopupMenu(emitter_add_menu,pt);
		return;
	}

}

void main::doOpen() {
	wxFileDialog fd(this, wxT("Open JSON game project"), "", "", "JSON files (*.json)|*.json", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

	if (fd.ShowModal() == wxID_CANCEL) return;

	//TODO: check if want to save current project
	doPopulateTree(fd.GetPath());

}

void main::doPopulateProps(int type, Json::Value _root) {
	
	prop_grid->grid->Freeze();

	switch (type) {
	case ItemData::CORE: {
		
		if (prop_grid->obj == wxT("core")) {

			prop_grid->SetFocus();
			break;
		}

		Json::Value cj = props_json["core"];
		for (Json::ValueIterator cc = cj.begin(); cc != cj.end(); ++cc) {

			Json::Value row = cj[cc.key().asString()];

			prop_grid->grid->Append(new wxPropertyCategory(cc.key().asString(), cc.key().asString()));

			for (unsigned int a = 0; a < row.size(); a++) {
				wxString n = row[a][0].asString();
				wxString t = row[a][1].asString();
				wxString v = row[a][2].asString();
				wxString d = row[a][3].asString();

				wxPGProperty *pt = NULL;
				if (t == wxT("str")) pt = new wxStringProperty(n, n);
				if (t == wxT("int")) pt = new wxIntProperty(n, n);
				if (t == wxT("bool")) pt = new wxBoolProperty(n, n);
				if (t == wxT("rgb")) pt = new wxColourProperty(n, n, wxColour(0, 0, 0));
				if (pt) {
					pt->SetHelpString(d);
					prop_grid->grid->Append(pt);
				}
			}

		}

		for (Json::ValueIterator op = _root["core"].begin(); op != _root["core"].end(); ++op) {
			Json::Value pl = (*op);
			//TODO not working
			if (pl.isString()) prop_grid->grid->SetPropertyValue(wxString(op.key().asString()+wxT("asdf")), wxString(pl.asString()));
			if (pl.isInt()) prop_grid->grid->SetPropertyValue(wxString(op.key().asString()), pl.asInt());
			if (pl.isBool()) prop_grid->grid->SetPropertyValue(wxString(op.key().asString()), pl.asBool());
			//TODO RGB
			//if (pl.isString()) prop_grid->grid->SetPropertyValue(op.key().asString, pl.asString())
		}

		prop_grid->obj = wxT("core");
	}
	break;
	default: {
		wxLogError(wxT("The properties of this type are not known."));
		break;
		}
	}

	prop_grid->grid->Thaw();
}

void main::doPopulateTree(wxString path) {

	main_notebook->DeleteAllPages();
	ptree->tree->DeleteAllItems();

	ptree->tree->Freeze();
	ItemData *cid = new ItemData;
	cid->type = ItemData::CORE;

	wxTreeItemId core_tid = ptree->tree->AddRoot(wxT("Project \"") + wxFileName(path).GetFullName() + wxT("\""), IMG_LOGO,-1,cid);
	graphics_tid = ptree->tree->AppendItem(core_tid, wxT("Graphics"), IMG_GRAPHIC);
	sounds_tid = ptree->tree->AppendItem(core_tid, wxT("Sounds"), IMG_SOUND);
	objects_tid = ptree->tree->AppendItem(core_tid, wxT("Objects"), IMG_OBJECT);
	emitters_tid = ptree->tree->AppendItem(core_tid, wxT("Emitters"), IMG_EMITTER);
	scenes_tid = ptree->tree->AppendItem(core_tid, wxT("Scenes"), IMG_SCENE);

	ptree->tree->SetItemBold(core_tid);
	ptree->tree->Expand(core_tid);

	ptree->tree->SetFocus();

	if (path.IsEmpty()) return;

	Json::Reader reader;

	wxSetWorkingDirectory(wxFileName(path).GetPath());

	//load metadata
	if (wxFileExists(path + wxT(".meta"))) {
		std::ifstream meta_doc(wxString(path + wxT(".meta")).c_str());
		bool ret = reader.parse(meta_doc, root);
		if (ret) {
			
			Json::Value filters_json = root["filters"];

			for (Json::ValueIterator i = filters_json.begin(); i != filters_json.end(); ++i) {
				wxString filter_key = i.key().asString();
				Json::Value graphics_filters_json = (*i);

				doFilterWalk(graphics_filters_json, graphics_tid, filter_key);
			}

		} else {
			wxLogError(wxString(reader.getFormattedErrorMessages()));
			wxLogError(wxT("Parsing JSON in file '" + wxFileName(path).GetFullName() + ".meta' failed."));
		}
	}

	//load project
	std::ifstream config_doc(path.c_str());
	//throw away first line to avoid parsing "var Game ="
	std::string foo;
	std::getline(config_doc, foo);

	bool ret = reader.parse(config_doc, root);
	if (!ret) {
		wxLogError(wxString(reader.getFormattedErrorMessages()));
		wxLogError(wxT("Parsing JSON in file '" + wxFileName(path).GetFullName() + "' failed."));

		main_notebook->DeleteAllPages();
		ptree->tree->DeleteAllItems();
		ptree->tree->Thaw();

		return;
	}

	core_json = root["core"];
	graphics_json = root["resources"]["graphics"];
	Json::Value sounds_json = root["sounds"];
	Json::Value objects_json = root["objects"];
	Json::Value scenes_json = root["scenes"];

	for (Json::ValueIterator i = graphics_json.begin(); i != graphics_json.end(); ++i) {
		ItemData *data = new ItemData;
		data->type = ItemData::GRAPHIC;

		wxTreeItemId pid = graphics_tid;

		for (sub_filter_hash_type::iterator b = filter_hash[wxT("graphics")].begin(); b != filter_hash[wxT("graphics")].end(); ++b) {
			wxString na = b->first;
			if (na == i.key().asString()) {
				pid = b->second;
				break;
			}
		}

		ptree->tree->AppendItem(pid, i.key().asString(), IMG_GRAPHIC_ITEM, -1, data);
	}

	for (Json::ValueIterator i = sounds_json.begin(); i != sounds_json.end(); ++i) {
		ItemData *data = new ItemData;
		data->type = ItemData::SOUND;
		ptree->tree->AppendItem(sounds_tid, i.key().asString(), IMG_SOUND_ITEM, -1, data);
	}

	for (Json::ValueIterator i = objects_json.begin(); i != objects_json.end(); ++i) {
		ItemData *data = new ItemData;
		data->type = ItemData::OBJECT;
		ptree->tree->AppendItem(objects_tid, i.key().asString(), IMG_OBJECT_ITEM, -1, data);
	}

	for (Json::ValueIterator i = scenes_json.begin(); i != scenes_json.end(); ++i) {
		ItemData *data = new ItemData;
		data->type = ItemData::SCENE;
		ptree->tree->AppendItem(scenes_tid, i.key().asString(), IMG_SCENE_ITEM, -1, data);
	}

	ptree->tree->Thaw();
}

void main::doFilterWalk(Json::Value j, wxTreeItemId p, wxString filter_key) {

	for (Json::ValueIterator a = j.begin(); a != j.end(); ++a) {
		wxString filter_name = a.key().asString();

		wxTreeItemId fid = ptree->tree->AppendItem(p, filter_name, IMG_FOLDER);

		if ((*a).isArray()) {
			for (unsigned int x = 0; x < (*a).size(); x++) {
				filter_hash[filter_key][(*a)[x].asString()] = fid;
			}
		} else {
			doFilterWalk((*a), fid, filter_key);
		}

	}

}

void main::onProjectTreeItemActivate(wxTreeEvent &event) {
	ItemData *foo = (ItemData*)ptree->tree->GetItemData(event.GetItem());
	if (foo) {
		if (foo->type == ItemData::CORE) {
			doPopulateProps(ItemData::CORE, root["core"]);
		}
		if (foo->type == ItemData::GRAPHIC) {
			wxString label = ptree->tree->GetItemText(event.GetItem());
			openGraphic(label, graphics_json[label]);
		}
		if (foo->type == ItemData::SCENE) {
			wxString label = ptree->tree->GetItemText(event.GetItem());
			openScene(label, root);
		}
	} else if (ptree->tree->ItemHasChildren(event.GetItem())) {
		if (ptree->tree->IsExpanded(event.GetItem()))
			ptree->tree->Collapse(event.GetItem());
		else
			ptree->tree->Expand(event.GetItem());
	}
}

void main::openScene(wxString name, Json::Value _root) {
	for (unsigned int i = 0; i < main_notebook->GetPageCount(); i++) {
		if (main_notebook->GetPage(i)->GetName() == name) {
			main_notebook->SetSelection(i);
			return;
		}
	}

	scene_editor *win = new scene_editor(main_notebook, name, _root);
	win->SetName(name);
	main_notebook->AddPage(win, name, true, IMG_SCENE);
}

void main::openGraphic(wxString name, Json::Value graphic_json) {

	for (unsigned int i = 0; i < main_notebook->GetPageCount(); i++) {
		if (main_notebook->GetPage(i)->GetName() == name) {

			main_notebook->SetSelection(i);
			return;
		}
	}

	tile_editor *win = new tile_editor(main_notebook, graphic_json, false);
	win->SetName(name);

	main_notebook->AddPage(win, name, true, IMG_GRAPHIC);
}

wxFileConfig *main::Config() {
	return new wxFileConfig(wxT("maple2d"));
}

main::~main() {
	
#ifndef _DEBUG
	wxFileConfig *conf = Config();
	conf->Write(wxT("general/perspective"), mgr.SavePerspective());
	delete conf;
#endif

	delete locale;

	mgr.UnInit();
}

void main::doNew() {

}

bool main::cacheProps() {

	//TODO make configurable
	wxFileName script_path(wxT("C:/dev/Maple2D/scripts"));

	//wxLogMessage(script_path.GetFullPath());

	return true;
}