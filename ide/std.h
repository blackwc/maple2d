#ifndef H_STD
#define H_STD

#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/menu.h>
#include <wx/aui/aui.h>
#include <wx/aui/auibook.h>
#include <wx/aui/auibar.h>

//#include <wx/splitter.h>
#include <wx/treectrl.h>
//#include <wx/mstream.h>
#include <wx/display.h>
#include <wx/propgrid/manager.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/property.h>
#include <wx/propgrid/advprops.h>
#include <wx/propgrid/props.h>
/*
#include <wx/stc/stc.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
*/
#include <wx/fileconf.h>
/*

#include <wx/process.h>
#include <wx/dir.h>
#include <wx/progdlg.h>
#include <wx/statline.h>
#include <wx/zipstrm.h>
*/

#include <wx/stdpaths.h>
#include <wx/dc.h>
#include <wx/dcbuffer.h>
#include <wx/graphics.h>
#include <wx/dcgraph.h>
#include <wx/mstream.h>

#include "glyphs_res.h"

#include "json/json.h"

#include "project_tree.h"
#include "properties_grid.h"
#include "scene_editor.h"
#include "tile_editor.h"

#include <fstream>
#include <string>
#include <vector>

#define APP_DISPLAY_NAME wxString(wxT("Maple 2D"))
#define APP_VENDOR_NAME wxString(wxT("igdblog.net"))

#define wxMEMORY_IMAGE( name ) _wxConvertMemoryToImage( name, sizeof( name ) )
#define wxMEMORY_IMAGEEX( name, type ) _wxConvertMemoryToImage( name, sizeof( name ), type )
#define wxMEMORY_BITMAP( name ) _wxConvertMemoryToBitmap( name, sizeof( name ) )
#define wxMEMORY_BITMAPEX( name, type ) _wxConvertMemoryToBitmap( name, sizeof( name ), type )

inline wxImage _wxConvertMemoryToImage(const unsigned char* data, int length, wxBitmapType type = wxBITMAP_TYPE_ANY)
{
	wxMemoryInputStream stream(data, length);
	return wxImage(stream, type, -1);
}

inline wxBitmap _wxConvertMemoryToBitmap(const unsigned char* data, int length, wxBitmapType type = wxBITMAP_TYPE_ANY)
{
	wxMemoryInputStream stream(data, length);
	return wxBitmap(wxImage(stream, type, -1), -1);
}

#endif