#ifndef H_APP
#define H_APP

#include "std.h"
#include "main.h"

class app : public wxApp
{
  public:
    virtual bool OnInit();
};

DECLARE_APP(app)

#endif