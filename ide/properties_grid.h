#ifndef H_PROPERTIES_GRID
#define H_PROPERTIES_GRID

#include "std.h"

class properties_grid : public wxPropertyGridManager {
public:

	wxPropertyGrid *grid;
	wxString obj;

	properties_grid(wxWindow *parent);
};

#endif