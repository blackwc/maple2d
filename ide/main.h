#ifndef H_MAIN
#define H_MAIN

#include "std.h"
#include "dockart.h"
#include "tabart.h"
#include "app.h"

class ItemData : public wxTreeItemData {
public:
	enum {
		GRAPHIC = 0,
		SOUND,
		OBJECT,
		SCENE,
		CORE
	};
	int type;
};

WX_DECLARE_STRING_HASH_MAP(wxTreeItemId, sub_filter_hash_type);
WX_DECLARE_STRING_HASH_MAP(sub_filter_hash_type, filter_hash_type);

class main : public wxFrame {
private:
	wxAuiManager mgr;

	wxLocale *locale;

	Json::Value root;
	Json::Value core_json;
	Json::Value graphics_json;
	Json::Value props_json;

	filter_hash_type filter_hash;
	sub_filter_hash_type sub_filter_hash;

	enum {
		MENU_EXIT = 6000,
		MENU_NEW,
		MENU_OPEN,
		MENU_HELP,
		MENU_ABOUT,
		MENU_SAVE,
		MENU_SAVEALL,
		MENU_SAVEAS,
		MENU_SETTINGS,
		MENU_ADD_GRAPHIC,
		MENU_ADD_SOUND,
		MENU_ADD_OBJECT,
		MENU_ADD_SCENE,
		MENU_ADD_FILTER,
		MENU_ADD_PARTICLE,
		MENU_ADD_LIGHT,
		MENU_ADD_SOUNDFX,
		MENU_ADD_EMITTER
	};

	enum {
		IMG_GRAPHIC = 0,
		IMG_SOUND,
		IMG_OBJECT,
		IMG_SCENE,
		IMG_GRAPHIC_ITEM,
		IMG_SOUND_ITEM,
		IMG_OBJECT_ITEM,
		IMG_SCENE_ITEM,
		IMG_LOGO,
		IMG_FOLDER,
		IMG_PARTICLE,
		IMG_LIGHT,
		IMG_SOUNDFX,
		IMG_EMITTER
	};

	wxAuiNotebook *main_notebook;
	properties_grid *prop_grid;
	project_tree *ptree;

	wxMenu *emitter_add_menu;
	wxAuiToolBar *project_tb;

	wxTreeItemId graphics_tid, sounds_tid, objects_tid, scenes_tid, emitters_tid;

	void doFilterWalk(Json::Value j, wxTreeItemId p, wxString filter_key);

public:

    main(const wxString &title);

	void onProjectTreeItemActivate(wxTreeEvent &event);
	void onCommand(wxCommandEvent &event);
	void onSize(wxSizeEvent &event);

	void openGraphic(wxString name, Json::Value _root);
	void openScene(wxString name, Json::Value _root);

	void onPageClosed(wxAuiNotebookEvent &event);
	void onPageChanged(wxAuiNotebookEvent &event);

	void doOpen();
	void doNew();

	bool cacheProps();

	void doPopulateTree(wxString path);
	void doPopulateProps(int type, Json::Value prop_object);

	wxFileConfig *Config();
	
	~main();
};

#endif