#include "app.h"

bool app::OnInit()
{
	wxInitAllImageHandlers();

	SetVendorName(APP_VENDOR_NAME);
	SetAppName(APP_DISPLAY_NAME);
	SetAppDisplayName(APP_DISPLAY_NAME);

	main *main_frm = new main(GetAppDisplayName());

    return true;
}

IMPLEMENT_APP(app)