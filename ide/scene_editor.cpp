#include "scene_editor.h"

scene_editor::scene_editor(wxWindow *parent, wxString name, Json::Value _root):
wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize) {
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	
	root = _root;

	Bind(wxEVT_PAINT, &scene_editor::onRender, this);


	//cache graphics
	wxArrayString foo_list;
	Json::Value graphics_json = root["scenes"][name]["graphics"];
	for (Json::ValueIterator i = graphics_json.begin(); i != graphics_json.end(); ++i) {
		if (foo_list.Index(i.key().asString()) == wxNOT_FOUND) foo_list.Add(i.key().asString());
	}

	graphics_json = root["graphics"];
	for (Json::ValueIterator i = graphics_json.begin(); i != graphics_json.end(); ++i) {
		if (foo_list.Index(i.key().asString()) != wxNOT_FOUND) {
			wxString res = (*i).get("res", "").asString();
			wxImage img;
			bool ret = img.LoadFile(res);
			if (!ret || res.IsEmpty()) continue;

			graphic g;
			g.img = img;
			g.x = 0;
			g.y = 0;
			g.z = 0;
			g.w = img.GetWidth();
			g.h = img.GetHeight();

			graphic_map gmap(i.key().asString(),g);
			graphics.push_back(gmap);
		}
	}

	//set the positions
	graphics_json = root["scenes"][name]["graphics"];
	for (std::vector<graphic_map>::iterator a = graphics.begin(); a != graphics.end(); ++a) {
		wxString n = (*a).first;
		graphic g = (*a).second;

		for (unsigned int i = 0; i < graphics_json[n].size(); i++) {
			int zindex = graphics_json[n][i].get("z", 0).asInt();
			Json::Value pos_list = graphics_json[n][i]["pos"];
			for (Json::ValueIterator p = pos_list.begin(); p != pos_list.end(); ++p) {
				Json::Value xy = (*p);
				g.x = xy[0].asInt();
				g.y = xy[1].asInt();
			}
		}
	}
}

void scene_editor::onRender(wxPaintEvent &event) {
	wxAutoBufferedPaintDC dc(this);
	wxGraphicsContext *gc = wxGraphicsContext::Create(dc);

	gc->SetBrush(wxSystemSettings::GetColour(wxSYS_COLOUR_APPWORKSPACE));

	gc->DrawRectangle(0, 0, GetClientRect().GetWidth(), GetClientRect().GetHeight());

	//render graphics
	for (std::vector<graphic_map>::iterator i = graphics.begin(); i != graphics.end(); ++i) {

		if((*i).second.img.IsOk()) gc->DrawBitmap(wxBitmap((*i).second.img), (*i).second.x, (*i).second.y, (*i).second.w, (*i).second.h);
	}
}