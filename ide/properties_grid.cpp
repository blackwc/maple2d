#include "properties_grid.h"

properties_grid::properties_grid(wxWindow *parent) :
wxPropertyGridManager(parent, wxID_ANY, wxDefaultPosition, wxSize(225, 400), wxPG_DESCRIPTION) {

	obj = wxEmptyString;

	grid = GetGrid();

	grid->GetGrid()->SetMarginColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
	grid->GetGrid()->SetCaptionBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
	grid->GetGrid()->SetCaptionTextColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	//pressing enter goes to next property
	grid->AddActionTrigger(wxPG_ACTION_NEXT_PROPERTY, WXK_RETURN);
	grid->DedicateKey(WXK_RETURN);
}