#include "tile_editor.h"

tile_editor::tile_editor(wxWindow *parent, Json::Value graphics_json, bool _tile_selector):
wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize) {

	tile_selector = _tile_selector;

	SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	Bind(wxEVT_PAINT, &tile_editor::onRender, this);
	Bind(wxEVT_SIZE, &tile_editor::onSize, this);

	if (tile_selector) {
		Bind(wxEVT_MOTION, &tile_editor::onMouseMove, this);
		Bind(wxEVT_LEFT_DOWN, &tile_editor::onMouseDown, this);
		Bind(wxEVT_LEFT_UP, &tile_editor::onMouseUp, this);
	}

	ready = false;
	dragging = false;
	prevRect = wxPoint(0, 0);
	mpos = wxPoint(0, 0);
	mclick = wxPoint(0, 0);
	rect = wxRect(0, 0, 0, 0);

	wxString res = graphics_json.asString();

	bool ret = img.LoadFile(res);
	if (!ret || res.IsEmpty()) {
		//TODO do something here if the image can't be loaded
	}

	
}

void tile_editor::onMouseMove(wxMouseEvent &event) {
	mpos = event.GetPosition();
	if (!ready) {
		if (rect.Contains(mpos)) {
			SetCursor(wxCURSOR_SIZING);
		} else {
			SetCursor(wxCURSOR_DEFAULT);
		}
	} else {
		//TODO on first hit to border it doesn't warp mouse properly
		dragging = true;
		int x = mpos.x - (mclick.x - prevRect.x);
		int y = mpos.y - (mclick.y - prevRect.y);
		if (x < 0) { x = 0; WarpPointer(x, mpos.y); }
		if (y < 0) { y = 0; WarpPointer(mpos.x, y); }
		if ((x + rect.GetWidth()) > img.GetWidth()) { x = (img.GetWidth() - rect.GetWidth() + (mclick.x - prevRect.x)); WarpPointer((img.GetWidth() - rect.GetWidth() + (mclick.x - prevRect.x)), mpos.y); }
		if ((y + rect.GetHeight()) > img.GetHeight()) { y = (img.GetHeight() - rect.GetHeight() + (mclick.y - prevRect.y)); WarpPointer(mpos.y, (img.GetHeight() - rect.GetHeight() + (mclick.y - prevRect.y))); }

		rect.SetPosition(wxPoint(x,y));

		RefreshRect(wxRect(0, 0, img.GetWidth(), img.GetHeight()));
		RefreshRect(wxRect(GetClientRect().GetWidth() - rect.GetWidth() * 5 - 20, 20, rect.GetWidth() * 5, rect.GetHeight() * 5));
	}
}

void tile_editor::onSize(wxSizeEvent &WXUNUSED(event)) {
	//TODO
	RefreshRect(wxRect(0, 0, img.GetWidth(), img.GetHeight()));
	GetParent()->Update();
}

void tile_editor::onMouseDown(wxMouseEvent &event) {

	if (event.GetButton() == wxMOUSE_BTN_LEFT && rect.Contains(mpos) && !dragging) {
		prevRect = wxPoint(rect.x,rect.y);
		SetCursor(wxCURSOR_BLANK);
		CaptureMouse();
		ready = true;
		mclick = event.GetPosition();
	}
}

void tile_editor::onMouseUp(wxMouseEvent &event) {
	if (event.GetButton() == wxMOUSE_BTN_LEFT && ready) {
		SetCursor(wxCURSOR_DEFAULT);
		ReleaseMouse();
		ready = false;
		dragging = false;
	}
}

void tile_editor::onRender(wxPaintEvent &event) {
	wxAutoBufferedPaintDC dc(this);
	wxGraphicsContext *gc = wxGraphicsContext::Create(dc);

	gc->SetBrush(wxSystemSettings::GetColour(wxSYS_COLOUR_APPWORKSPACE));
	gc->DrawRectangle(0, 0, GetClientRect().GetWidth(), GetClientRect().GetHeight());

	wxBrush brush;
	brush.SetStipple(wxMEMORY_IMAGE(alpha_pattern_png));
	gc->SetBrush(brush);
	gc->DrawRectangle(0, 0, img.GetWidth(), img.GetHeight());

	gc->SetBrush(*wxTRANSPARENT_BRUSH);

	if (img.IsOk()) {

		if (tile_selector) {
			gc->BeginLayer(1);
			gc->DrawBitmap(img.ConvertToGreyscale(), 0, 0, img.GetWidth(), img.GetHeight());

			wxPen pen;
			pen.SetColour(wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHT));
			pen.SetWidth(2);
			gc->SetPen(pen);
			gc->DrawBitmap(img.GetSubImage(rect), rect.x, rect.y, rect.GetWidth(), rect.GetHeight());
			gc->DrawRectangle(rect.x, rect.y, rect.GetWidth(), rect.GetHeight());

		
			gc->BeginLayer(1);

			gc->SetBrush(brush);
			gc->DrawRectangle(GetClientRect().GetWidth() - rect.GetWidth() * 5 - 20, 20, rect.GetWidth() * 5, rect.GetHeight() * 5);

			gc->SetBrush(*wxTRANSPARENT_BRUSH);

			wxImage tmg = img.GetSubImage(wxRect(rect.x, rect.y, rect.GetWidth(), rect.GetHeight())).Rescale(rect.GetWidth() * 5, rect.GetHeight() * 5);
			if (tmg.IsOk()) gc->DrawBitmap(tmg, GetClientRect().GetWidth() - rect.GetWidth() * 5 - 20, 20, tmg.GetWidth(), tmg.GetHeight());

			gc->DrawRectangle(GetClientRect().GetWidth() - rect.GetWidth() * 5 - 20, 20, tmg.GetWidth(), tmg.GetHeight());
		} else {
			gc->BeginLayer(1);
			gc->DrawBitmap(img, 0, 0, img.GetWidth(), img.GetHeight());
		}
	}
}