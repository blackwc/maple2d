#ifndef H_GRAPHIC_EDITOR
#define H_GRAPHIC_EDITOR

#include "std.h"

class tile_editor : public wxScrolledWindow {
	wxPoint mpos;
	wxPoint mclick;
	bool ready, dragging, tile_selector;
	wxPoint prevRect;
	wxRect rect;
	wxImage img;
public:
	tile_editor(wxWindow *parent, Json::Value graphic_json, bool tile_selector);
	void onRender(wxPaintEvent &event);
	void onMouseDown(wxMouseEvent &event);
	void onMouseMove(wxMouseEvent &event);
	void onMouseUp(wxMouseEvent &event);
	void onSize(wxSizeEvent &event);
};

#endif